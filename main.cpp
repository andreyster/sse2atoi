#include <iostream>
#include <emmintrin.h>
#include <stdint.h>
#include <cstdlib>
#include "strtk/strtk.hpp"
#include <boost/lexical_cast.hpp>

#define USE_INLINE_ASM

template <int N>
struct ReverseImm8 {
    static const int value = (ReverseImm8<N - 1>::value << 2) | (N - 1);
};
template <>
struct ReverseImm8<0> {
    static const int value = 0;
};

#define noinline  __attribute__(( noinline ))

/// Computes log2(N)
template <typename T, T N, T P = 1, bool DONE = ((1 << P) >= N)> struct LOG2 {
    static const T value = LOG2<T, N, P + 1>::value;
};
template <typename T, T N, T P> struct LOG2<T, N, P, true> {
    static const T value = P;
};

template <int N>
struct Int2Type {
    static const int value = N;
};

template <unsigned int N, typename T>
struct int_pow10;

template <typename T>
struct int_pow10<0, T> {
    static const T value = 1;
};
template <unsigned int N, typename T>
struct int_pow10 {
    static const T value = static_cast<T>(int_pow10<N - 1, T>::value  * 10llu);
};

__m128i movd(uint32_t v) {
    __m128i result;
    __asm__("movd %1, %0\n" : "=x" (result): "r" (v));
    return result;
}

#if defined(__INTEL_COMPILER)
#define mkvec(X) _mm_set1_epi32(X)
typedef __m128i __v4si;
#else
#define mkvec(X) {X, X, X, X}
#endif

/// Round an integer constant to the next power of 2.
/// \note Use V - 1 if you want the template to return
///       the same value if it's a power of 2.
template <typename T, T V, unsigned int SHIFT = sizeof(T) * 4>
struct round_to_pow2 {
    static const T value = round_to_pow2<T, V | (V >> SHIFT), SHIFT / 2>::value;
};

template <typename T, T V>
struct round_to_pow2<T, V, 1> {
    static const T value =  (V | (V >> 1)) + 1 + (V == 0);
};

const __v4si ascii_zeros __attribute__((aligned (16))) = mkvec(~0u / 255 * '0');
const __v4si mult100 __attribute__((aligned (16))) = mkvec((1 << 16) | 100);
const __v4si mult10000 __attribute__((aligned (16))) = mkvec((1 << 16) | 10000);
//const __v4si mult100000000 = { 100000000, 0, 1, 0 };
#if defined(__INTEL_COMPILER)
//const __v4si mult100000000 = _mm_set_epi32( 100000000, 0, 1, 0 );
const __v4si mult100000000 __attribute__((aligned (16))) = _mm_set_epi32( 0, 1, 0, 100000000 );
#else
const __v4si mult100000000 __attribute__((aligned (16))) = { 100000000, 0, 1, 0 };
#endif

template <int N>
static __m128i shift_digits(__m128i digits) {
    asm("pslldq	%[shift], %[digits]\n"
        : [digits] "+x" (digits)
        : [shift] "i" (N)
    );
    return digits;
}

inline static __m128i process_up_to(Int2Type<4>, __m128i & digits) {
    __m128i even_digits;
    __m128i mult100reg;
    __asm__(
        // TODO: swap digits and even digits to allow better pairing
        "	movdqa	%[digits], %[even_digits]\n"
        "	movdqa	%[mult100], %[mult100reg]\n"
        // drop least significant digit by shifting by 8:
        //               15                        0
        // even_digits = | 3 | 2 | 1 | 0 | ... | 0 |
        // multiply even digits by 10 using shifts and add them to odd digits
        // x * 10 == x * 8 + x * 2 == x << 3 + x << 1
        //               15                                0
        //      digits = |  4 |  3 |  2 |  1 | 0 | ... | 0 |
        //                         +
        // even_digits = | 30 | 20 | 10 | 00 | 0 | ... | 0 |
        //                         =
        // digits      = | 34 | 23 | 12 | 01 | 0 | ... | 0 |
        "	psllw	$9, %[even_digits]\n" // drop odd (8) + mul by 2  (1) = 9
        "	paddb	%[even_digits], %[digits]\n"
        "	psllw	$2, %[even_digits]\n" // multiply by 4 (2) to get x * 8
        "	paddb	%[even_digits], %[digits]\n"
        // convert double-digits to words
        //               15                                0
        // digits      = | 00 | 34 | 00 | 12 | 0 | ... | 0 |
        "	psrlw	$8, %[digits]\n"
        // merge double-digits into 32-bit words
        //               15                   11                   7     0
        // mult100     = | 1       | 100      | 1       | 100      | ... |
        // digits      = | 1 * 34  + 100 * 12 | 1 * 0   + 100 * 0  | ... |
        // TODO: investigate if generating mult100 is faster
        "	pmaddwd	%[mult100reg], %[digits]\n"
        : [digits] "+x" (digits), [even_digits] "=x" (even_digits), [mult100reg] "=x" (mult100reg)
        : [mult100] "m" (mult100)
        // : [shift] "i" (N)
    );
    return digits;
}

inline static __m128i process_up_to(Int2Type<8>, __m128i & digits) {
    __m128i mult10000reg;
    asm(
        // load 10000 multiplicator into an xmm register
        "	movdqa		%[mult10000], %[mult10000reg]\n"
        : [mult10000reg] "=x" (mult10000reg)
        : [mult10000] "m" (mult10000)
    );
    digits = ::process_up_to(Int2Type<4>(), digits);
    asm(
        // shrink 32-bit dwords into 16-bit words
        "	packssdw	%[digits], %[digits]\n"
        // merge the 4-digit 16-bit words
        "	pmaddwd		%[mult10000reg], %[digits]\n"
        : [digits] "+x" (digits)
        : [mult10000reg] "x" (mult10000reg)
    );
    return digits;
}
inline static __m128i process_up_to(Int2Type<16>, __m128i & digits) {
    digits = ::process_up_to(Int2Type<8>(), digits);
    __m128i upper_digits;
    asm(
        // swap 2 upper 32-bit dwords.
        "	pshufd	%[shuffle], %[digits], %[digits]\n"
        // multiply upper 32-bit dword by 100000000
        "	pmuludq	%[mult100000000], %[digits]\n"
        // extract upper 32bit dword into [upper_digits]
        "	movdqa	%[digits], %[upper_digits]\n"
        "	psrldq	$8, %[upper_digits]\n"
        // add words
        "	paddq	%[upper_digits], %[digits]\n"
       : [digits] "+x" (digits),
         [upper_digits] "=x" (upper_digits)
       : [mult100000000] "m" (mult100000000),
         [shuffle] "i" ((2 << 6) | (3 << 4) | (1 << 2) | 0)
    );
    return digits;
}

template <typename T>
inline static T convert_up_to(Int2Type<4>, __m128i digits) {
    uint32_t result;
    digits = ::process_up_to(Int2Type<4>(), digits);
    asm(
	"	pextrw	$6, %[digits], %[result]\n"
        : [result] "=r" (result), [digits] "+x" (digits)
    );
    return result;
}

template <typename T>
inline static T extract_dword(__m128i   digits) {
    uint32_t result_low;
    uint32_t result;
    asm(
	// "	pextrw	$7, %[digits], %[high]\n"
	// "	pextrw	$6, %[digits], %[low]\n"
	// "	sall	$16, %[high]\n"
	// "	orl	%[low], %[high]\n"
        "	psrldq	$12, %[digits]\n"
	"	movd	%[digits], %[high]\n"
        : [low] "=r" (result_low), [high] "=r" (result), [digits] "+x" (digits)
    );
    return result;
}

template <typename T>
inline static T convert_up_to(Int2Type<8>, __m128i   digits) {
    digits = ::process_up_to(Int2Type<8>(), digits);
    uint32_t result_low;
    uint32_t result;
    asm(
	// "	pextrw	$7, %[digits], %[high]\n"
	// "	pextrw	$6, %[digits], %[low]\n"
	// "	sall	$16, %[high]\n"
	// "	orl	%[low], %[high]\n"
        "	psrldq	$12, %[digits]\n"
	"	movd	%[digits], %[high]\n"
        : [low] "=r" (result_low), [high] "=r" (result), [digits] "+x" (digits)
    );
    return result;
}

template <typename T>
inline static T convert_up_to(Int2Type<16>, __m128i  digits, Int2Type<32>) {
    digits = ::process_up_to(Int2Type<16>(), digits);
    uint32_t result;
    asm(
	"	movd	%[digits], %[result]\n"
        : [result] "=r" (result), [digits] "+x" (digits)
    );
    return result;
}

template <typename T>
inline static T convert_up_to(Int2Type<16>, __m128i  digits, Int2Type<64>) {
    digits = ::process_up_to(Int2Type<16>(), digits);
    uint32_t result_low;
    uint32_t result_high;
    asm(
	"	movd	%[digits], %[low]\n"
        "	psrldq	$4, %[digits]\n"
	"	movd	%[digits], %[high]\n"
        : [low] "=r" (result_low), [high] "=r" (result_high)
        : [digits] "x" (digits)
    );
    return static_cast<T>((static_cast<T>(result_high) << 32llu) | result_low);
}


template <int N, typename T>
class SSE2AtoI {
public:

//    static __m128i shift_digits(__m128i digits) {
//        return _mm_slli_si128(digits, 16 - N);
//    }

    static __m128i  process_up_to(Int2Type<4>, __m128i & digits) {
#ifdef USE_INLINE_ASM
        digits = shift_digits<16 - N>(digits);
        return ::process_up_to(Int2Type<4>(), digits);
#endif
        // drop least significant digit by shifting by 8:
        //               15                        0
        // even_digits = | 3 | 2 | 1 | 0 | ... | 0 |
        // multiply even digits by 10 using shifts and add them to odd digits
        // x * 10 == x * 8 + x * 2 == x << 3 + x << 1
        //               15                                0
        //      digits = |  4 |  3 |  2 |  1 | 0 | ... | 0 |
        //                         +
        // even_digits = | 30 | 20 | 10 | 00 | 0 | ... | 0 |
        //                         =
        // digits      = | 34 | 23 | 12 | 01 | 0 | ... | 0 |
        __m128i even_digits = _mm_slli_epi16(digits, 8 /* drop odd */ + 1 /* mul by 2 */);
        digits = _mm_add_epi8(digits, even_digits);
        even_digits = _mm_slli_epi16(even_digits, 2 /* mul by 4 */);
        digits = _mm_add_epi8(digits, even_digits);
        // convert double-digits to words
        //               15                                0
        // digits      = | 00 | 34 | 00 | 12 | 0 | ... | 0 |
        digits = _mm_srli_epi16(digits, 8);
        // generate multipliers to merge double digits into 32-bit words:
        //               15                                           0
        // mult100     = | 1       | 100     | 1       | 100    | ... |
//        __m128i mult100 = _mm_set1_epi32((1 << 16) | 100);
//        const __m128i mult100 = mkvec((1 << 16) | 100);
        //               15                  11                 7     0
        // digits      = | 1 * 34 + 100 * 12 | 1 * 0 + 100 * 0  | ... |
        digits = _mm_madd_epi16(digits, (__m128i &)mult100);
        return digits;
    }

    static T convert_up_to(Int2Type<1>, __m128i   digits) {
        digits = shift_digits<16 - N>(digits);
#ifdef USE_INLINE_ASM
        return ::convert_up_to<T>(Int2Type<4>(), digits);
#endif
        return _mm_extract_epi16(process_up_to(Int2Type<4>(), digits), 6);
    }

    static T convert_up_to(Int2Type<2>, __m128i   digits) {
        digits = shift_digits<16 - N>(digits);
#ifdef USE_INLINE_ASM
        return ::convert_up_to<T>(Int2Type<4>(), digits);
#endif
        return _mm_extract_epi16(process_up_to(Int2Type<4>(), digits), 6);
    }

    static T convert_up_to(Int2Type<4>, __m128i   digits) {
        digits = shift_digits<16 - N>(digits);
#ifdef USE_INLINE_ASM
        return ::convert_up_to<T>(Int2Type<4>(), digits);
#endif
        return _mm_extract_epi16(process_up_to(Int2Type<4>(), digits), 6);
    }

    static __m128i  process_up_to(Int2Type<8>, __m128i & digits) {
#ifdef USE_INLINE_ASM
        digits = shift_digits<16 - N>(digits);
        return ::process_up_to(Int2Type<8>(), digits);
#endif
        digits = ::process_up_to(Int2Type<4>(), digits);

        // shrink 32-bit dwords into 16-bit words
        digits = _mm_packs_epi32(digits, digits);
        // merge the 4-digit 16-bit words
        digits = _mm_madd_epi16(digits, (__m128i &)mult10000);
        return digits;
    }

    static T convert_up_to(Int2Type<8>, __m128i   digits) {
        digits = shift_digits<16 - N>(digits);
#ifdef USE_INLINE_ASM
        return ::convert_up_to<T>(Int2Type<8>(), digits);
#endif
        return _mm_cvtsi128_si32(_mm_srli_si128(process_up_to(Int2Type<8>(), digits), 12));
    }

    static __m128i  process_up_to(Int2Type<16>, __m128i & digits) {
#ifdef USE_INLINE_ASM
        digits = shift_digits<16 - N>(digits);
        return ::process_up_to(Int2Type<16>(), digits);
#endif
        digits = process_up_to(Int2Type<8>(), digits);
        // swap 2 upper 32-bit dwords.
        digits = _mm_shuffle_epi32(digits, (2 << 6) | (3 << 4) | (1 << 2) | 0);
        // multiply and add them together
        digits = _mm_mul_epu32(digits, _mm_set_epi32(0, 1, 0, 100000000));
        __m128i upper_digits = _mm_srli_si128(digits, 8);
        digits = _mm_add_epi64(digits, upper_digits);
        return digits;
    }

    static T convert_up_to(Int2Type<16>, __m128i  digits, Int2Type<32>) {
        digits = shift_digits<16 - N>(digits);
#ifdef USE_INLINE_ASM
        return ::convert_up_to<T>(Int2Type<16>(), digits, Int2Type<32>());
#endif
        return _mm_cvtsi128_si32(process_up_to(Int2Type<16>(), digits));
    }

    static T convert_up_to(Int2Type<16>, __m128i  digits, Int2Type<64>) {
        digits = shift_digits<16 - N>(digits);
#ifdef USE_INLINE_ASM
        return ::convert_up_to<T>(Int2Type<16>(), digits, Int2Type<64>());
#endif
        digits = process_up_to(Int2Type<16>(), digits);
        uint32_t low_word = _mm_cvtsi128_si32(digits);
        digits = _mm_srli_si128(digits, 4);
        uint32_t high_word = _mm_cvtsi128_si32(digits);
        return (static_cast<T>(high_word) << 32) | low_word;
    }

    static T convert_up_to(Int2Type<16>, __m128i  digits) {
        return convert_up_to(Int2Type<16>(), digits, Int2Type<(sizeof(T) > 4 ? 64 : 32)>());
    }

    static T convert(__m128i  digits) {
        return convert_up_to(Int2Type<round_to_pow2<int, N - 1>::value>(), digits);
    }

};

/**
 * Load number into n1 and n2 and calculate it's length.
 */
unsigned int load_num(const char * s, __m128i & n1, __m128i & n2, __m128i & temp) {
    unsigned int len;
    asm (
        "\n"
        // str = "1234\0"
        "	movdqu	%[str], %[n1]\n"
        //      15                               0
        // n1 = |...| \0 | '4' | '3' | '2' | '1' |
        "	pxor	%[temp], %[temp]\n"
        // temp = 0
        "	pcmpeqb	%[n1], %[temp]\n"
        // convert ASCII to to numbers
        "	psubb	%[az], %[n1]\n"
        // temp = temp == n1
        "	pmovmskb	%[temp], %[len]\n"
        // add stop bit in case there are more than 15 digits
        // NOTE: not needed if sure the length
        "	or	%[sb],	%[len]\n"
        // len= 0b0000001xxxxxxxxxxxxxxx
        "	bsfl	%[len], %[len]\n"
        : [temp] "=x" (temp),
          [n1] "=x" (n1),
          [n2] "=x" (n2),
          [len] "=r" (len)
        : [str] "m" (*s),
          [sb] "i" (1 << 16),
          [az] "m" (ascii_zeros)
    );
    return len;
}

template <typename T>
T sse2atoi(const char * a) noinline;
template <typename T>
T sse2atoi(const char * a) {
    __m128i digits;
    __m128i n2;
    __m128i temp;
    unsigned int length = load_num(a, digits, n2, temp);
    // "1234\0"
    //       15                               0
    // str = |...| \0 | '4' | '3' | '2' | '1' |
//    uintptr_t addr = reinterpret_cast<uintptr_t>(a);
//    uintptr_t misalignment = addr & 0xF;
//    __m128i str = _mm_load_si128(reinterpret_cast<const __m128i*>(addr & ~0xF));
//    __m128i str = _mm_loadu_si128(reinterpret_cast<const __m128i*>(a));
//    register __m128i zero;
//    zero = _mm_xor_si128(zero, zero);
//    __m128i cmp = _mm_cmpeq_epi8(str, zero);
//    int length = __builtin_ctz(_mm_movemask_epi8(cmp));// >> misalignment) + misalignment;
    // generate all '0' vector
//    __m128i ascii_zeros = _mm_set1_epi32(~0u / 255 * '0');
    // convert ascii to to numbers
//    register __m128i digits = _mm_sub_epi8(str, (__m128i&)ascii_zeros);
#if 1
    static void * jumptbl[] = { 0, &&case_1, &&case_2, &&case_3, &&case_4, &&case_5, &&case_6, &&case_7, &&case_8, &&case_9, &&case_10, &&case_11, &&case_12, &&case_13, &&case_14, &&case_15, &&case_16 };
    goto *jumptbl[length];
        case_1:  return SSE2AtoI<1 , uint32_t>::convert(digits);
        case_2:  return SSE2AtoI<2 , uint32_t>::convert(digits);
        case_3:  return SSE2AtoI<3 , uint32_t>::convert(digits);
        case_4:  return SSE2AtoI<4 , uint32_t>::convert(digits);
        case_5:  return SSE2AtoI<5 , uint32_t>::convert(digits);
        case_6:  return SSE2AtoI<6 , uint32_t>::convert(digits);
        case_7:  return SSE2AtoI<7 , uint32_t>::convert(digits);
        case_8:  return SSE2AtoI<8 , uint32_t>::convert(digits);
        case_9:  return SSE2AtoI<9 , T       >::convert(digits);
        case_10: return SSE2AtoI<10, T       >::convert(digits);
        case_11: return SSE2AtoI<11, T       >::convert(digits);
        case_12: return SSE2AtoI<12, T       >::convert(digits);
        case_13: return SSE2AtoI<13, T       >::convert(digits);
        case_14: return SSE2AtoI<14, T       >::convert(digits);
        case_15: return SSE2AtoI<15, T       >::convert(digits);
        case_16: return SSE2AtoI<16, T       >::convert(digits);
#endif
#if 0
        case_16: {
            T result = SSE2AtoI<16, T>::convert(digits);
            for (;;) {
                a += 16;
                length = load_num(a, digits, n2, temp);
                switch (length) {
                    default:
                    case 0:     return result;
                    case 1:     return result * int_pow10< 1, T>::value + SSE2AtoI< 1, uint32_t>::convert(digits);
                    case 2:     return result * int_pow10< 2, T>::value + SSE2AtoI< 2, uint32_t>::convert(digits);
                    case 3:     return result * int_pow10< 3, T>::value + SSE2AtoI< 3, uint32_t>::convert(digits);
                    case 4:     return result * int_pow10< 4, T>::value + SSE2AtoI< 4, uint32_t>::convert(digits);
                    case 5:     return result * int_pow10< 5, T>::value + SSE2AtoI< 5, uint32_t>::convert(digits);
                    case 6:     return result * int_pow10< 6, T>::value + SSE2AtoI< 6, uint32_t>::convert(digits);
                    case 7:     return result * int_pow10< 7, T>::value + SSE2AtoI< 7, uint32_t>::convert(digits);
                    case 8:     return result * int_pow10< 8, T>::value + SSE2AtoI< 8, uint32_t>::convert(digits);
                    case 9:     return result * int_pow10< 9, T>::value + SSE2AtoI< 9, T       >::convert(digits);
                    case 10:    return result * int_pow10<10, T>::value + SSE2AtoI<10, T       >::convert(digits);
                    case 11:    return result * int_pow10<11, T>::value + SSE2AtoI<11, T       >::convert(digits);
                    case 12:    return result * int_pow10<12, T>::value + SSE2AtoI<12, T       >::convert(digits);
                    case 13:    return result * int_pow10<13, T>::value + SSE2AtoI<13, T       >::convert(digits);
                    case 14:    return result * int_pow10<14, T>::value + SSE2AtoI<14, T       >::convert(digits);
                    case 15:    return result * int_pow10<15, T>::value + SSE2AtoI<15, T       >::convert(digits);
                    case 16:  result = result * int_pow10<16, T>::value + SSE2AtoI<16, T       >::convert(digits);
                }
            }
         }
#endif
#if 0
//        default: return 0;
    switch (length) {
        case 1:  return SSE2AtoI<1 , T>::convert(digits);
        case 2:  return SSE2AtoI<2 , T>::convert(digits);
        case 3:  return SSE2AtoI<3 , T>::convert(digits);
        case 4:  return SSE2AtoI<4 , T>::convert(digits);
        case 5:  return SSE2AtoI<5 , T>::convert(digits);
        case 6:  return SSE2AtoI<6 , T>::convert(digits);
        case 7:  return SSE2AtoI<7 , T>::convert(digits);
        case 8:  return SSE2AtoI<8 , T>::convert(digits);
        case 9:  return SSE2AtoI<9 , T>::convert(digits);
        case 10: return SSE2AtoI<10, T>::convert(digits);
        case 11: return SSE2AtoI<11, T>::convert(digits);
        case 12: return SSE2AtoI<12, T>::convert(digits);
        case 13: return SSE2AtoI<13, T>::convert(digits);
        case 14: return SSE2AtoI<14, T>::convert(digits);
        case 15: return SSE2AtoI<15, T>::convert(digits);
        case 16: return SSE2AtoI<16, T>::convert(digits);
//        default: return 0;
    }
#endif
#if 0
    // shift all digits to the most significant byte:
    //          15                            0
    // digits = | 4 | 3 | 2 | 1 | 0 | ... | 0 |
    switch (length) {
        case 1:  digits = _mm_slli_si128(digits, 15); break;
        case 2:  digits = _mm_slli_si128(digits, 14); break;
        case 3:  digits = _mm_slli_si128(digits, 13); break;
        case 4:  digits = _mm_slli_si128(digits, 12); break;
        case 5:  digits = _mm_slli_si128(digits, 11); break;
        case 6:  digits = _mm_slli_si128(digits, 10); break;
        case 7:  digits = _mm_slli_si128(digits, 9 );  break;
        case 8:  digits = _mm_slli_si128(digits, 8 );  break;
        case 9:  digits = _mm_slli_si128(digits, 7 );  break;
        case 10: digits = _mm_slli_si128(digits, 6 );  break;
        case 11: digits = _mm_slli_si128(digits, 5 );  break;
        case 12: digits = _mm_slli_si128(digits, 4 );  break;
        case 13: digits = _mm_slli_si128(digits, 3 );  break;
        case 14: digits = _mm_slli_si128(digits, 2 );  break;
        case 15: digits = _mm_slli_si128(digits, 1 );  break;
        default: break;
    }
    // drop least significant digit by shifting by 8:
    //               15                        0
    // even_digits = | 3 | 2 | 1 | 0 | ... | 0 |
    // multiply even digits by 10 using shifts and add them to odd digits
    // x * 10 == x * 8 + x * 2 == x << 3 + x << 1
    //               15                                0
    //      digits = |  4 |  3 |  2 |  1 | 0 | ... | 0 |
    //                         +
    // even_digits = | 30 | 20 | 10 | 00 | 0 | ... | 0 |
    //                         =
    // digits      = | 34 | 23 | 12 | 01 | 0 | ... | 0 |
    __m128i even_digits = _mm_slli_epi16(digits, 8 /* drop odd */ + 1 /* mul by 2 */);
    digits = _mm_add_epi8(digits, even_digits);
    even_digits = _mm_slli_epi16(even_digits, 2 /* mul by 4 */);
    digits = _mm_add_epi8(digits, even_digits);
    // convert double-digits to words
    //               15                                0
    // digits      = | 00 | 34 | 00 | 12 | 0 | ... | 0 |
    digits = _mm_srli_epi16(digits, 8);
    // generate multipliers to merge double digits into 32-bit words:
    //               15                                           0
    // mult100     = | 1       | 100     | 1       | 100    | ... |
    __m128i mult100 = _mm_set1_epi32((1 << 16) | 100);
    //               15                  11                 7     0
    // digits      = | 1 * 34 + 100 * 12 | 1 * 0 + 100 * 0  | ... |
    digits = _mm_madd_epi16(digits, mult100);
    // shrink 32-bit dwords into 16-bit words
    digits = _mm_packs_epi32(digits, digits);
    // merge the 4-digit 16-bit words
    __m128i mult10000 = _mm_set1_epi32((1 << 16) | 10000);
    digits = _mm_madd_epi16(digits, mult10000);
    // swap 2 upper 32-bit dwords.
    digits = _mm_shuffle_epi32(digits, (2 << 6) | (3 << 4) | (1 << 2) | 0);
    // multiply and add them together
    __m128i mult100000000 = _mm_set_epi64x(1, 100000000);
    digits = _mm_mul_epu32(digits, mult100000000);
    __m128i upper_digits = _mm_srli_si128(digits, 8);
    digits = _mm_add_epi64(digits, upper_digits);
    return _mm_cvtsi128_si32(digits);
#endif
}

inline __m128i load_digits(const char * s) {
    __m128i digits;
    asm (
        "	movdqu	%[str], %[digits]\n"
        "	psubb	%[az], %[digits]\n"
        : [digits] "=x" (digits)
        : [str] "m" (*s),
          [az] "m" (ascii_zeros)
    );
    return digits;
}

inline unsigned int convert3digits(const char * s) {
    unsigned int d3 = s[0];
    unsigned int d2 = s[1];
    unsigned int d1 = s[2];
    asm (
        "	leal	(%[d3],%[d3],4), %[d3]\n"
        //      d3 = d3 + d3 * 4 = d3 * 5
        "	leal	-528(%[d2],%[d3],2), %[d3]\n"
        //      d3 = d2 + d3 * 2 - '0' * 10 - '0'  =  d3 * 10 - '0' * 10 + d2 - '0'
        "	leal	(%[d3],%[d3],4), %[d3]\n"
        //      d3 = d3 + d3 * 4
        "	leal	-48(%[d1],%[d3],2), %[d1]\n"
        //      d1 = d3 * 2 + d1 - '0' = d3 * 10 + d1 - '0'
        : [d1] "+r" (d1),
          [d2] "+r" (d2),
          [d3] "+r" (d3)
    );
    return d1;
}

template <unsigned int N, typename T, unsigned int IDX = N>
struct AtoI {
    static const T value(const char a[], T sum = 0) {
//        return (a[IDX] - '0') * int_pow10<N - IDX - 1, T>::value + AtoI<N, T, IDX + 1>::value(a);
        return AtoI<N, T, IDX - 1>::value(a, sum * 10 + a[IDX - 1] - '0');
    }
};

template <unsigned int N, typename T>
struct AtoI<N, T, 0> {
    static const T value(const char a[], T result) { return result; }
};

template <typename T>
T sse2atoi(const char * a, unsigned int length) noinline;
template <typename T>
T sse2atoi(const char * a, unsigned int length) {
    static void * jumptbl[] = { 0, &&case_1, &&case_2, &&case_3, &&case_4, &&case_5, &&case_6, &&case_7, &&case_8, &&case_9, &&case_10, &&case_11, &&case_12, &&case_13, &&case_14, &&case_15, &&case_16 };
//    __m128i digits = load_digits(a);
    goto *jumptbl[length];
        case_1:  return *a - '0';
//        case_1:  return atoi_expr<1, T>::value(a);
        case_2:  return (a[0] - '0') * 10 + (a[1] - '0');
        case_3:  return (a[0] - '0') * 100 + (a[1] - '0') * 10 + (a[2] - '0');
//        case_3:  return convert3digits(a);
        case_4:  return (a[0] - '0') * 1000 + (a[1] - '0') * 100 + (a[2] - '0') * 10 + (a[3] - '0');
//        case_4:  return SSE2AtoI<4 , uint32_t>::convert(load_digits(a));
        case_5:  return (a[0] - '0') * 10000 + (a[1] - '0') * 1000 + (a[2] - '0') * 100 + (a[3] - '0') * 10 + (a[4] - '0');
//        case_5:  return SSE2AtoI<5 , uint32_t>::convert(load_digits(a));
//        case_6:  return (a[0] - '0') * 100000 + (a[1] - '0') * 10000 + (a[2] - '0') * 1000 + (a[3] - '0') * 100 + (a[4] - '0') * 10 + (a[5] - '0');
        case_6:  return SSE2AtoI<6 , uint32_t>::convert(load_digits(a));
        case_7:  return SSE2AtoI<7 , uint32_t>::convert(load_digits(a));
        case_8:  return SSE2AtoI<8 , uint32_t>::convert(load_digits(a));
        case_9:  return SSE2AtoI<9 , T       >::convert(load_digits(a));
        case_10: return SSE2AtoI<10, T       >::convert(load_digits(a));
        case_11: return SSE2AtoI<11, T       >::convert(load_digits(a));
        case_12: return SSE2AtoI<12, T       >::convert(load_digits(a));
        case_13: return SSE2AtoI<13, T       >::convert(load_digits(a));
        case_14: return SSE2AtoI<14, T       >::convert(load_digits(a));
        case_15: return SSE2AtoI<15, T       >::convert(load_digits(a));
        case_16: return SSE2AtoI<16, T       >::convert(load_digits(a));
}

uint64_t rdtsc() {
#ifndef __LP64__
    uint64_t result;
    __asm__ __volatile__("rdtscp" : "=A" (result));
    return result;
#else
    uint32_t low, high;
    __asm__ __volatile__("rdtscp" : "=a" (low), "=d" (high));
    return low | ((uint64_t)high << 32);
#endif
}

template <typename T>
T loopatoi(const char * a) noinline;
template <typename T>
T loopatoi(const char * a) {
    T result = 0;
    while (*a) {
        result = result * 10 + (*a - '0');
        ++a;
    }
    return result;
}

int empty(const char * a) noinline;
int empty(const char * a) {
    return 0;
}

int strtk_num_conv(const char * a) noinline;
int strtk_num_conv(const char * a) {
    int x;
    strtk::fast::numeric_convert(__builtin_strlen(a), a, x, false);
    return x;
}

int strtk_num_conv(const char * a, unsigned int len) noinline;
int strtk_num_conv(const char * a, unsigned int len) {
    int x;
    strtk::fast::numeric_convert(len, a, x, false);
    return x;
}


const char data[] __attribute__((aligned (16))) = "1234567891";
const char small_data[] __attribute__((aligned (16))) = "1234";
//const char * volatile buf __attribute__((aligned (16)))   = "0000000000000000012345678901";
const char * volatile buf = data;
int main(int argc, char **argv) {
    const char * volatile ptr = buf;
    std::cout << sse2atoi<uint32_t>(data, 6) << std::endl;
    while (*ptr) {
        int x;
        std::cout << sse2atoi<uint64_t>(ptr++) << std::endl;
    }
//    argc = sse2atoi("12345");
    static const int ITER = 100000000;
    uint64_t start = rdtsc();
    for (int i = 0; i < ITER; ++i) {
        argc += empty(buf);
    }
    uint64_t looptime = rdtsc() - start;
    start = rdtsc();
    for (int i = 0; i < ITER; ++i) {
        argc += sse2atoi<int>(buf);
    }
    uint64_t time = rdtsc() - start - looptime;
    std::cout << "sse2 ticks=" << time / ITER << std::endl;
    start = rdtsc();
    for (int i = 0; i < ITER; ++i) {
        argc += loopatoi<int>(buf);
    }
    time = rdtsc() - start - looptime;
    std::cout << "loop ticks=" << time / ITER << std::endl;

    start = rdtsc();
    for (int i = 0; i < ITER; ++i) {
        argc += strtk_num_conv(buf);
    }
    time = rdtsc() - start - looptime;
    std::cout << "strtk ticks=" << time / ITER << std::endl;
    start = rdtsc();
    for (int i = 0; i < ITER; ++i) {
        argc += ::atoi(buf);
    }
    time = rdtsc() - start - looptime;
    std::cout << "atoi ticks=" << time / ITER << std::endl;
    for (int n = 1; n < sizeof(data); ++n) {
        start = rdtsc();
        for (int i = 0; i < ITER; ++i) {
            argc += sse2atoi<int>(buf, n);
        }
        time = rdtsc() - start - looptime;
        std::cout << "fixed length of " << n << " sse2 ticks=" << (double)time / ITER << std::endl;
    }
    for (int n = 1; n < sizeof(data); ++n) {
        start = rdtsc();
        for (int i = 0; i < ITER; ++i) {
            argc += strtk_num_conv(buf, n);
        }
        time = rdtsc() - start - looptime;
        std::cout << "fixed length of " << n << " strtk ticks=" << (double)time / ITER << std::endl;
    }
    for (int n = 1; n < sizeof(data); ++n) {
        start = rdtsc();
        for (int i = 0; i < ITER; ++i) {
            argc += boost::lexical_cast<int>(buf + n - 1);
        }
        time = rdtsc() - start - looptime;
        std::cout << "lexical_cast ticks=" << time / ITER << std::endl;
    }
    return argc;

}

// EOF
